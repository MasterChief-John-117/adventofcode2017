﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AdventOfCode
{
    class Day2
    {
        public string Part1()
        {
            int sum = 0;
            foreach(var line in File.ReadAllLines("inputs/day2"))
            {
                sum += line.Split().Max(d => int.Parse(d)) - line.Split().Min(d => int.Parse(d));
            }
            return sum.ToString();
        }

        public string Part2()
        {
            int sum = 0;
            foreach (var line in File.ReadAllLines("inputs/day2"))
            {
                foreach(var c in line.Split())
                {
                    foreach(var d in line.Split())
                    {
                        if (c!= d && double.Parse(c) % double.Parse(d) == 0)
                        {
                            sum += int.Parse(c) / int.Parse(d);
                        }
                        if (c != d && double.Parse(d) % double.Parse(c) == 0)
                        {
                            sum += int.Parse(d) / int.Parse(c);
                        }
                    }
                }
            }
            return (sum/2).ToString();
        }
    }
}
