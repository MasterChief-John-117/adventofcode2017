﻿using System;

namespace AdventOfCode
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Day 1 Part 1: {new Day1().Part1()}");
            Console.WriteLine($"Day 1 Part 2: {new Day1().Part2()}");
            Console.WriteLine($"Day 2 Part 1: {new Day2().Part1()}");
            Console.WriteLine($"Day 2 Part 2: {new Day2().Part2()}");
            Console.WriteLine($"Day 3 Part 1: {new Day3().Part1()}");
            Console.WriteLine($"Day 3 Part 2: {new Day3().Part2()}");
            Console.WriteLine($"Day 4 Part 1: {new Day4().Part1()}");
            Console.WriteLine($"Day 4 Part 2: {new Day4().Part2()}");
            Console.WriteLine($"Day 5 Part 1: {new Day5().Part1()}");
            Console.WriteLine($"Day 5 Part 2: {new Day5().Part2()}");
            Console.WriteLine($"Day 6 Part 1: {new Day6().Part1()}");
            Console.WriteLine($"Day 6 Part 2: {new Day6().Part2()}");
            Console.WriteLine($"Day 7 Part 1: {new Day7().Part1()}");
            Console.WriteLine($"Day 7 Part 2: {new Day7().Part2()}");
            Console.WriteLine($"Day 8 Part 1: {new Day8().Part1()}");
            Console.WriteLine($"Day 8 Part 2: {new Day8().Part2()}");
            Console.WriteLine($"Day 9 Part 1: {new Day9().Part1()}");
            Console.WriteLine($"Day 9 Part 2: {new Day9().Part2()}");
            Console.WriteLine($"Day 10 Part 1: {new Day10().Part1()}");
            Console.WriteLine($"Day 10 Part 2: {new Day10().Part2()}");
            Console.WriteLine($"Day 11 Part 1: {new Day11().Part1()}");
            Console.WriteLine($"Day 11 Part 2: {new Day11().Part2()}");
            Console.WriteLine($"Day 12 Part 1: {new Day12().Part1()}");
            Console.WriteLine($"Day 12 Part 2: {new Day12().Part2()}");
            Console.WriteLine($"Day 13 Part 1: {new Day13().Part1()}");
            Console.WriteLine($"Day 13 Part 2: {new Day13().Part2()}");
            Console.WriteLine($"Day 14 Part 1: {new Day14().Part1()}");
            Console.WriteLine($"Day 14 Part 2: {new Day14().Part2()}");
            Console.WriteLine($"Day 15 Part 1: {new Day15().Part1()}");
            Console.WriteLine($"Day 15 Part 2: {new Day15().Part2()}");
            Console.WriteLine($"Day 16 Part 1: {new Day16().Part1()}");
            Console.WriteLine($"Day 16 Part 2: {new Day16().Part2()}");
            Console.WriteLine($"Day 17 Part 1: {new Day17().Part1()}");
            Console.WriteLine($"Day 17 Part 2: {new Day17().Part2()}");
            Console.WriteLine($"Day 18 Part 1: {new Day18().Part1()}");
            Console.WriteLine($"Day 18 Part 2: {new Day18().Part2()}");
            Console.WriteLine($"Day 19 Part 1: {new Day19().Part1()}");
            Console.WriteLine($"Day 19 Part 2: {new Day19().Part2()}");
            Console.WriteLine($"Day 20 Part 1: {new Day20().Part1()}");
            Console.WriteLine($"Day 20 Part 2: {new Day20().Part2()}");
            Console.WriteLine($"Day 21 Part 1: {new Day21().Part1()}");
            Console.WriteLine($"Day 21 Part 2: {new Day21().Part2()}");
            Console.WriteLine($"Day 22 Part 1: {new Day22().Part1()}");
            Console.WriteLine($"Day 22 Part 2: {new Day22().Part2()}");
            Console.WriteLine($"Day 23 Part 1: {new Day23().Part1()}");
            Console.WriteLine($"Day 23 Part 2: {new Day23().Part2()}");
            Console.WriteLine($"Day 24 Part 1: {new Day24().Part1()}");
            Console.WriteLine($"Day 24 Part 2: {new Day24().Part2()}");
            Console.WriteLine($"Day 25 Part 1: {new Day25().Part1()}");
            Console.WriteLine($"Day 25 Part 2: {new Day25().Part2()}");
            Console.ReadKey();
        }
    }
}
