﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AdventOfCode
{
    class Day1
    {
        public string Part1()
        {
            string input = File.ReadAllText("inputs/day1");
            int sum = 0;
            for(int i = 0; i < input.Length-1; i++)
            {
                if (input[i] == input[i+1])
                    sum += int.Parse(input[i].ToString());
            }
            if (input[0] == input[input.Length - 1])
                sum += int.Parse(input[0].ToString());

            return sum.ToString();
        }

        public string Part2()
        {
            string input = File.ReadAllText("inputs/day1");
            int sum = 0;
            for (int i = 0; i < input.Length/2; i++)
            {
                if (input[i] == input[i + (input.Length/2)])
                    sum += int.Parse(input[i].ToString())*2;
            }
            return sum.ToString();
        }
    }
}
